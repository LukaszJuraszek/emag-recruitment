<?php

namespace tests\Emag\Lib\Parser;

use Emag\Lib\Parser\EmagSchemaPriceParser;
use PHPUnit\Framework\TestCase;

/**
 * Class EmagSchemaPriceParserTest
 * @package test\Emag\Lib\Parser
 */
class EmagSchemaPriceParserTest extends TestCase
{
    /**
     * @var EmagSchemaPriceParser
     */
    private $parser;
    
    public function setUp()
    {
        $this->parser = new EmagSchemaPriceParser(__DIR__ . '/../../../../xml/example.xml');
    }
    
    public function testParse()
    {
        $productsPriceData = $this->parser->parse();
        
        $this->assertInternalType('array', $productsPriceData);
        
        $expectedValue = [
            'ABCDE13292'  => 23.48,
            'ABCDEP00005' => 18.79,
            'ABCDEP00007' => 45.23
        ];
        
        $this->assertEquals($expectedValue, $productsPriceData);
    }
    
    public function tearDown()
    {
        $this->parser = null;
    }
}
