<?php

namespace tests\Emag\Lib\Getter;

use Emag\Lib\Getter\HttpGetter;
use PHPUnit\Framework\TestCase;

class HttpGetterTest extends TestCase
{
    /**
     * @var HttpGetter
     */
    private $getter;
    
    /**
     * @var string
     */
    private $file;
    
    public function setUp()
    {
        $this->getter = new HttpGetter('http://google.pl');
    }
    
    public function testGetData()
    {
        $this->file = $this->getter->getData();
        
        $this->assertInternalType('string', $this->file);
        
        $this->assertFileExists($this->file);
    }
    
    public function tearDown()
    {
        unlink($this->file);
        
        $this->file = '';
        
        $this->getter = null;
    }
}
