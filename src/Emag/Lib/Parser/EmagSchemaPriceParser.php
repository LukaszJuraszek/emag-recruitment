<?php

declare(strict_types=1);

namespace Emag\Lib\Parser;

use Symfony\Component\DomCrawler\Crawler;

/**
 * Class EmagSchemaPriceParser
 * @package Emag\Lib\Parser
 */
class EmagSchemaPriceParser
{
    /**
     * @var string
     */
    protected $filePath;
    
    /**
     * @var Crawler
     */
    protected $crawler;
    
    /**
     * EmagSchemaPriceParser constructor.
     *
     * @param string $filePath path to file
     */
    public function __construct($filePath)
    {
        $this->filePath = $filePath;
        $this->crawler  = new Crawler(file_get_contents($this->filePath));
    }

    /**
     * parses XML taken from $this->filePath and extracts products price data
     * @return array
     */
    public function parse(): array
    {
        /** @var array $productsPriceData */
        $productsPriceData = [];
    
        foreach ($this->crawler->filterXPath('//offer/price') as $price)
        {
            if (null !== $product = $price->parentNode->getAttribute('partNo'))
            {
                $productsPriceData[$product] = (float) $price->textContent;
            }
        }
        
        return $productsPriceData;
    }
}
