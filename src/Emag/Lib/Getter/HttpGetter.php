<?php

declare(strict_types=1);

namespace Emag\Lib\Getter;

/**
 * Class HttpGetter
 * @package Emag\Lib\Getter
 */
class HttpGetter
{
    const DOWNLOADED_FILE_PATH = __DIR__ . '/../../../../var/uploads/';
    
    /**
     * @var string
     */
    private $file;
    
    /**
     * @var string
     */
    protected $url;
    
    /**
     * HttpGetter constructor.
     *
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * downloads data from $this->url URL
     * @throws \Exception
     * @return string $data downloaded data
     */
    public function getData()
    {
        if ($this->upload($this->url))
        {
            return $this->file;
        }
        
        throw new \Exception('Unable to download data');
    }
    
    /**
     * @param string $url
     *
     * @return bool
     */
    private function upload($url): bool
    {
        if ('octet-stream' !== $this->getExtension())
        {
            $this->file = self::DOWNLOADED_FILE_PATH . date('YmdHis') . '.' . $this->getExtension();
            
            return copy($url, $this->file);
        }
        
        return false;
    }
    
    /**
     * @return string
     */
    private function getContentType(): string
    {
        $headers = get_headers($this->url);
        
        foreach ($headers as $header)
        {
            if (preg_match('/^Content-Type/', $header))
            {
            	$temp = explode(':', $header);
            	
            	$temp = substr($temp[1], 0, strpos($temp[1], ';'));
            	
            	return trim($temp);
            }
        }
        
        return 'application/octet-stream';
    }
    
    /**
     * @return string
     */
    private function getExtension(): string
    {
        $mimeType = $this->getContentType();
        
        $temp = explode('/', $mimeType);
        
        return $temp[1];
    }
}
